
//3
fetch("https://jsonplaceholder.typicode.com/todos")
	.then(response => response.json())
	.then(json => console.log(json));
// //4
fetch("https://jsonplaceholder.typicode.com/todos")
      .then(response => response.json())
      // .then(json => {json.map(post =>console.log(post.title))});
      .then(json => console.log(json.map(post => post.title)));

//5
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => console.log(json))
//6
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`))

// //7
fetch("https://jsonplaceholder.typicode.com/todos",
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					title: "Created To Do List Item",
					userId: 1
				})
			}
		)
	.then(response => response.json())
	.then(json => console.log(json));

// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.


// 9. Update a to do list item by changing the data structure to contain the following properties:
/*- Title
- Description
- Status
- Date Completed
- User ID*/
fetch("https://jsonplaceholder.typicode.com/todos/1",{
		method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			description: "To update the my to do list with a different data structure",
			status: "Pending",
			dateCompleted: "Pending",
			title: "Updated To Do List Item",
			userId: 1
		})
	})
	.then(response => response.json())
	.then(json => console.log(json));
// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.
fetch("https://jsonplaceholder.typicode.com/todos/1",{
		method: "PATCH",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			dateCompleted: "07/09/21",
			status: "Complete"
		})
	})
	.then(response => response.json())
	.then(json => console.log(json));
// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", {method: "DELETE"})

	.then(response => response.json())
	.then(json => console.log(json))